const Readline = require("./Readline.js");
class Task1 {
  constructor(operator) {
    this.rl = new Readline.Readline();
    this.operator = operator;
  }
  async setAngka() {
    this.firstNumber = await this.rl.input("Masukkan angka pertama: ");
    this.secondNumber = await this.rl.input("Masukkan angka kedua: ");
  }
  async hitungakarKuadrat() {
    let number = await this.rl.input("Masukkan angka: ");
    const result = Math.sqrt(+number);
    return await this.rl.output(`Akar kuadrat dari ${number} adalah ${result} \n`);
  }
  async calculator(operator) {
    let result = 0;
    try {
      switch (operator) {
        case "+":
          await this.setAngka();
          result = +this.firstNumber + +this.secondNumber;
          break;
        case "-":
          await this.setAngka();
          result = +this.firstNumber - +this.secondNumber;
          break;
        case "/":
          await this.setAngka();
          result = +this.firstNumber / +this.secondNumber;
          break;
        case "*":
          await this.setAngka();
          result = +this.firstNumber * +this.secondNumber;
          break;
        default:
          process.exit(1);
      }
      await this.rl.output(`Hasil dari ${this.firstNumber} ${operator} ${this.secondNumber} adalah ${result}\n`);
    } catch (error) {
      console.log(error);
    }
  }
  async luasPersegi() {
    const number = await this.rl.input('Masukkan panjang sisi: ');
    const result = (+(number**2));
    return await this.rl.output(`Volume Kubus dengan sisi ${number} adalah ${result}\n`);
  }
  async volumeKubus() {
    const number = await this.rl.input('Masukkan panjang sisi: ');
    const result = (+(number**3));
    return await this.rl.output(`Luas Persegi dengan sisi ${number} adalah ${result}\n`);
  }
  async volumeTabung() {
    const jariJari = await this.rl.input('Masukkan panjang jari-jari: ');
    const tinggi = await this.rl.input('Masukkan tinggi: ');
    const result = 3.14 * +(jariJari**2) * +tinggi ;
    return await this.rl.output(`Volume tabung dengan jari-jari ${jariJari} dan tinggi ${tinggi} adalah ${result}\n`);
  }

}

let Test = new Task1();
async function main() {
  await Test.calculator("+");
  await Test.hitungakarKuadrat();
  await Test.luasPersegi();
  await Test.volumeKubus();
  await Test.volumeTabung();
  process.exit();
}
main();
