class Readline {
  
  constructor() {
    const readline = require("readline");
    this.interface = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
  }
  input(question) {
    return new Promise ((resolve, reject) => {
        try {
          this.interface.question((question), data => {
            return resolve(data);
          });
        } catch (error) {
          reject(error);
        }
    });
  }
 async output(data) {
    return new Promise (async (resolve, reject) => {
      try {
        const result = this.interface.write(data.toString());
        resolve(result);
      } catch (error) {
        reject(error);
      }
    })
  }
}
module.exports = { Readline }
 